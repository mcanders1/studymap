var redIcon = L.icon({
    iconUrl: 'img/icons/location_red_small.png',
    iconSize: [38,55],
    iconAnchor: [19, 54],
    popupAnchor: [0, -55]
});

var dummyLibraryData = {
    name: 'Grainger',
    hours: '8:30am - 12:00am',
    address: '1301 W Springfield Ave, Urbana, IL 61801',
    longLat: [40.1125,-88.226917],
    color: '#7ad0e9',
    populationData: [{"time":"00","population":"3"},
                    {"time":"02","population":"4"},
                    {"time":"04","population":"2"},
                    {"time":"06","population":"10"},
                    {"time":"08","population":"60"},
                    {"time":"10","population":"10"},
                    {"time":"12","population":"20"},
                    {"time":"14","population":"30"},
                    {"time":"16","population":"20"},
                    {"time":"18","population":"40"},
                    {"time":"20","population":"50"},
                    {"time":"22","population":"58"}],
    floors: [
        {
            name: 'Lower Level',
            index: 0,
            color: '#e77b61',
            emptySeats: 107, //array of coordinates,
            takenSeats: 84, //array of coordinates
            populationData: [{"time":"00","population":"3"},
                            {"time":"02","population":"4"},
                            {"time":"04","population":"2"},
                            {"time":"06","population":"10"},
                            {"time":"08","population":"60"},
                            {"time":"10","population":"10"},
                            {"time":"12","population":"20"},
                            {"time":"14","population":"30"},
                            {"time":"16","population":"20"},
                            {"time":"18","population":"40"},
                            {"time":"20","population":"50"},
                            {"time":"22","population":"58"}]
        },
        {
            name: 'First Floor',
            index: 1,
            color: '#1dbaa7',
            emptySeats: 55, //array of coordinates,
            takenSeats: 23, //array of coordinates
            populationData: [{"time":"00","population":"3"},
                            {"time":"02","population":"4"},
                            {"time":"04","population":"2"},
                            {"time":"06","population":"10"},
                            {"time":"08","population":"60"},
                            {"time":"10","population":"10"},
                            {"time":"12","population":"20"},
                            {"time":"14","population":"30"},
                            {"time":"16","population":"20"},
                            {"time":"18","population":"40"},
                            {"time":"20","population":"50"},
                            {"time":"22","population":"58"}]
        },
        {
            name: 'Second Floor',
            index: 2,
            color: '#eccb57',
            emptySeats: 42, //array of coordinates,
            takenSeats: 104, //array of coordinates
            populationData: [{"time":"00","population":"3"},
                            {"time":"02","population":"4"},
                            {"time":"04","population":"2"},
                            {"time":"06","population":"10"},
                            {"time":"08","population":"60"},
                            {"time":"10","population":"10"},
                            {"time":"12","population":"20"},
                            {"time":"14","population":"30"},
                            {"time":"16","population":"20"},
                            {"time":"18","population":"40"},
                            {"time":"20","population":"50"},
                            {"time":"22","population":"58"}]
        },
        {
            name: 'Third Floor',
            index: 3,
            color: '#7ad0e9',
            emptySeats: 67, //array of coordinates,
            takenSeats: 10, //array of coordinates
            populationData: [{"time":"00","population":"3"},
                            {"time":"02","population":"4"},
                            {"time":"04","population":"2"},
                            {"time":"06","population":"10"},
                            {"time":"08","population":"60"},
                            {"time":"10","population":"10"},
                            {"time":"12","population":"20"},
                            {"time":"14","population":"30"},
                            {"time":"16","population":"20"},
                            {"time":"18","population":"40"},
                            {"time":"20","population":"50"},
                            {"time":"22","population":"58"}]
        },
        {
            name: 'Fourth Floor',
            index: 4,
            color: 'rgb(204,199,180)',
            emptySeats: 32, //array of coordinates,
            takenSeats: 12, //array of coordinates
            populationData: [{"time":"00","population":"3"},
                            {"time":"02","population":"4"},
                            {"time":"04","population":"2"},
                            {"time":"06","population":"10"},
                            {"time":"08","population":"60"},
                            {"time":"10","population":"10"},
                            {"time":"12","population":"20"},
                            {"time":"14","population":"30"},
                            {"time":"16","population":"20"},
                            {"time":"18","population":"40"},
                            {"time":"20","population":"50"},
                            {"time":"22","population":"58"}]
        },
    ]
};


var app = {
    // Application Constructor
    initialize: function() {
        this.map = null;
        this.popups = {};
        this.libraries = {};
        this.selectedLibrary = null;


        this.bindEvents();

        this.initMap();

        this.addButton('Preference', '#e77b61', 'preference_red', 'preference_white');
        this.addButton('Nearby', '#eccb57', 'nearby_yellow', 'nearby_white');
        this.addButton('Confirm', '#1dbaa7', 'confirm_green', 'confirm_white', this.confirmButton.bind(this));
        this.addButton('Cancel', '#7ad0e9', 'cancel_blue', 'cancel_white', this.cancelButton.bind(this));

        this.addLibrary(dummyLibraryData);  

        this.selectedLibrary = this.libraries['Grainger'];
        this.popups['Grainger'].openPopup();

    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {

    },
    cancelButton: function() {
        this.hideAllPopups();
        this.selectedLibrary = null;
    },
    confirmButton: function() {
        if(this.selectedLibrary !== null) {
            this.hideMapView();
            this.selectedLibrary.showOverview();
        }
    },
    initMap: function() {
        this.map = L.map('map', {zoomControl: false}).setView([40.1125,-88.226917], 16);
        this.map.on('click', function() {app.cancelButton();});
        L.tileLayer.grayscale('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);    
    },
    addButton: function(text, color, icon_color_name, icon_white_name, onclick) {
        var buttons = document.getElementById('buttons');
        var button = document.createElement('div');
        button.className = 'button';

        var buttonHighlight = document.createElement('div');
        buttonHighlight.className = 'buttonHighlight';
        buttonHighlight.style.background = color;

        var buttonBody = document.createElement('div');
        buttonBody.className = 'buttonBody';

        var buttonText = document.createElement('div');
        buttonText.textContent = text;
        buttonText.className = 'buttonText';

        var buttonImage = document.createElement('img');
        buttonImage.src = 'img/icons/'+icon_color_name+'.png';
        buttonImage.className = 'buttonImage';

        buttonBody.appendChild(buttonImage);
        buttonBody.appendChild(buttonText);
        button.appendChild(buttonHighlight);
        button.appendChild(buttonBody);

        buttons.appendChild(button);

        buttonBody.addEventListener('touchstart', function() {
            buttonBody.style.background = color;
            buttonImage.src = 'img/icons/'+icon_white_name+'.png';
            buttonText.style.color = 'rgb(247, 248, 252)';
        });
        buttonBody.addEventListener('touchend', function() {
            buttonText.style.color = 'black';
            buttonImage.src = 'img/icons/'+icon_color_name+'.png';
            buttonBody.style.background = "rgb(247, 248, 252)";
            if(onclick) {
                onclick();
            }
        });
        
        // buttonBody.addEventListener('click', function() {
        //     buttonBody.style.background = color;
        //     buttonImage.src = 'img/icons/'+icon_white_name+'.png';
        //     buttonText.style.color = 'rgb(247, 248, 252)';
        //     if(onclick) {
        //         onclick();
        //     }
        // });
    },
    hideAllPopups: function() {
        for(var x in this.popups) {
            this.popups[x].closePopup();
        }
    },
    hideMapView: function() {
        document.getElementById('selection').style.display = 'none';
    },
    addLibrary: function(data) {
        var marker = L.marker(data.longLat, {icon: redIcon}).addTo(this.map);
        var popup = marker.bindPopup("<div style='font-size:1.5em;font-weight:700;'>"+
            data.name+"</div>"+data.hours+"<br>"+data.address+"<br>");
        this.popups[data.name] = popup;
        var library = new Library(data);
        this.libraries[data.name] = library;
        marker.on('click', function() {
            app.hideAllPopups();
            app.selectedLibrary = library;
            popup.openPopup();
        });
    },

};

app.initialize();