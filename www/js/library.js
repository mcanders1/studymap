function Library(data, titleImage) {
	this.floors = {};
	this.createOverview(data);

	for(var floorIndex in data.floors) {
		this.addFloor(data.floors[floorIndex]);
	}
}

Library.prototype.createOverview = function(data) {
	var container = document.createElement('div');
	var title = document.createElement('div');
	title.className = 'titleImage';
	title.textContent = data.name;

	var middleContent = document.createElement('div');
	middleContent.className = 'middleContent';

	var time = document.createElement('div');
	time.className = 'time';
	time.textContent = 'CURRENT TIME: 18:42';

	var floorButtonContainer = document.createElement('div');
	floorButtonContainer.className = 'floorButtonContainer';

	// var emptyBarGraph = document.createElement('div');
	// emptyBarGraph.className = 'emptyBarGraph';


	middleContent.appendChild(time);
	middleContent.appendChild(floorButtonContainer);
	// middleContent.appendChild(emptyBarGraph);

	var bottomGraph = document.createElement('div');
	bottomGraph.className = 'bottomGraph';
	bottomGraph.setAttribute("id", "bottomGraphOverview");

	
	container.appendChild(title);
	container.appendChild(middleContent);
	container.appendChild(bottomGraph);
  

	document.getElementById('libraries').appendChild(container);


	var	graph_data = data.populationData;
	
	
	var margin = {top: 20, right: 20, bottom: 40, left: 30},
	width = 270 ,
	height = 100;
	
	var parseDate = d3.time.format("%X").parse;
	
	var x = d3.scale.linear()
	.domain(graph_data.map(function (d) {return d.time;}))
	.range([0, width]);
	
	var y = d3.scale.linear()
	.range([height, 0]);
	
	var xAxis = d3.svg.axis()
	.scale(x)
	.orient("bottom")
	;
	
	var yAxis = d3.svg.axis()
	.scale(y)
	.orient("left");
	
	var area = d3.svg.area()
	.interpolate("monotone")
	.x(function(d) { return x(d.time); })
	.y0(height)
	.y1(function(d) { return y(d.population); });
	
	var svg = d3.select("#bottomGraphOverview").append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
	.append("g")
	.style("fill", function(d) { return data.color; })

	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	

	
	graph_data.forEach(function(d) {
				 d.time = +(d.time);
				 d.population = +d.population;
				 });
				 
				 x.domain(d3.extent(graph_data, function(d) { return d.time; }));
				 y.domain([0, d3.max(graph_data, function(d) { return d.population; })]);
				 
				 svg.append("path")
				 .datum(graph_data)
				 .attr("class", "area")
				 .attr("d", area);
				 
				 svg.append("g")
				 .attr("class", "x axis")
				 .attr("transform", "translate(0," + 100 + ")")
				 .call(xAxis)
				 .append("text")
				 .attr("x", 250)
				 .attr("y", -5)
				 .attr("dx", "0.71em")
				 .style("text-anchor", "end")
				 .style("fill", "white")
				 .text("Time");
				 
				 svg.append("g")
				 .attr("class", "y axis")
				 .call(yAxis)
				 .append("text")
				 .attr("transform", "rotate(-90)")
				 .attr("y", 5)
				 .attr("x", 10)
				 .attr("dy", ".71em")
				 .style("text-anchor", "end")
				 .text("Population");


	this.floorButtons = floorButtonContainer;
	this.overviewContainer = container;

	this.hideOverview();
};

Library.prototype.addFloor = function(data) {
	var floor = new Floor(data);
	var that = this;
	this.addFloorButton(data.name, data.emptySeats, data.color,
		function() {floor.showFloorInfo(); that.hideOverview()});
};

Library.prototype.addFloorButton = function(text, emptySpots, color, onclick) {
    var button = document.createElement('div');
    button.className = 'floorButton';
    var buttonHighlight = document.createElement('div');
    buttonHighlight.className = 'floorButtonHighlight';
    buttonHighlight.style.background = color;

    var buttonBody = document.createElement('div');
    buttonBody.className = 'floorButtonBody';

    var buttonText = document.createElement('div');
    buttonText.textContent = text;
    buttonText.className = 'floorButtonText';

    var buttonEmptySpots = document.createElement('div');
    buttonEmptySpots.textContent = 'Empty Spots: ' + emptySpots;
    buttonEmptySpots.className = 'floorButtonEmptySpots';

    button.appendChild(buttonHighlight);
    buttonBody.appendChild(buttonText);
    buttonBody.appendChild(buttonEmptySpots);
    button.appendChild(buttonBody);

    this.floorButtons.appendChild(button);

    buttonBody.addEventListener('touchstart', function() {
        buttonBody.style.background = color;
        buttonText.style.color = 'rgb(247, 248, 252)';
        buttonEmptySpots.style.color = 'rgb(247, 248, 252)';


    });
    buttonBody.addEventListener('touchend', function() {
        buttonText.style.color = 'black';
        buttonEmptySpots.style.color = 'black';
        buttonBody.style.background = "rgb(247, 248, 252)";
        if(onclick) {
            onclick();
        }
    });
	
		// buttonBody.addEventListener('click', function() {
  //       buttonBody.style.background = color;
  //       buttonText.style.color = 'rgb(247, 248, 252)';
  //       buttonEmptySpots.style.color = 'rgb(247, 248, 252)';
  //       if(onclick) {
  //           onclick();
  //       }
  //   });
};

Library.prototype.showOverview = function() {
	this.overviewContainer.style.display = 'block';
};

Library.prototype.hideOverview = function() {
	this.overviewContainer.style.display = 'none';
};
