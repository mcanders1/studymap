function Floor(data) {
	this.createFloorInfo(data);
}

Floor.prototype.createFloorInfo = function(data) {
	var container = document.createElement('div');
	var title = document.createElement('div');
	title.className = 'titleImage';
	title.textContent = data.name;

	var middleContent = document.createElement('div');
	middleContent.className = 'middleContent';
	middleContent.setAttribute("id", "middleContent"+data.index);

	var time = document.createElement('div');
	time.className = 'floorTime';
	//time.textContent = 'CURRENT TIME: 18:42';


	middleContent.appendChild(time);

	var bottomGraph = document.createElement('div');
	bottomGraph.className = 'bottomGraph';
	bottomGraph.style = 'width:100%; height:100%;';
	bottomGraph.setAttribute("id", "bottomGraph"+data.index);

	
	//remove this when done
	
	container.appendChild(title);
	container.appendChild(middleContent);
	container.appendChild(bottomGraph);

	document.getElementById('floors').appendChild(container);
	
	
	var	graph_data = data.populationData;
	
	
	var margin = {top: 20, right: 20, bottom: 40, left: 30},
	width = 270 ,
	height = 100;
	
	var parseDate = d3.time.format("%X").parse;
	
	var x = d3.scale.linear()
	.domain(graph_data.map(function (d) {return d.time;}))
	.range([0, width]);
	
	var y = d3.scale.linear()
	.range([height, 0]);
	
	var xAxis = d3.svg.axis()
	.scale(x)
	.orient("bottom")
	;
	
	var yAxis = d3.svg.axis()
	.scale(y)
	.orient("left");
	
	var area = d3.svg.area()
	.interpolate("monotone")
	.x(function(d) { return x(d.time); })
	.y0(height)
	.y1(function(d) { return y(d.population); });
	
	var svg = d3.select("#bottomGraph"+data.index).append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
	.append("g")
	.style("fill", function(d) { return data.color; })

	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	

	
	graph_data.forEach(function(d) {
				 d.time = +(d.time);
				 d.population = +d.population;
				 });
				 
				 x.domain(d3.extent(graph_data, function(d) { return d.time; }));
				 y.domain([0, d3.max(graph_data, function(d) { return d.population; })]);
				 
				 svg.append("path")
				 .datum(graph_data)
				 .attr("class", "area")
				 .attr("d", area);
				 
				 svg.append("g")
				 .attr("class", "x axis")
				 .attr("transform", "translate(0," + 100 + ")")
				 .call(xAxis)
				 .append("text")
				 .attr("x", 250)
				 .attr("y", -5)
				 .attr("dx", "0.71em")
				 .style("text-anchor", "end")
				 .style("fill", "white")
				 .text("Time");
				 
				 svg.append("g")
				 .attr("class", "y axis")
				 .call(yAxis)
				 .append("text")
				 .attr("transform", "rotate(-90)")
				 .attr("y", 5)
				 .attr("x", 10)
				 .attr("dy", ".71em")
				 .style("text-anchor", "end")
				 .text("Population");
				


				 
	var graph_data = [{"type":"Occupied", "population":data.takenSeats},
				{"type":"Empty", "population":data.emptySeats}];
	
	var containerWidth = middleContent.clientWidth;
	var width = containerWidth/3,
	height = containerWidth/3,
	radius = Math.min(width, height) / 2;
	
	var color = d3.scale.ordinal()
	.range(["#F8F9FB", data.color]);
	
	var arc = d3.svg.arc()
	.outerRadius(radius - 10)
	.innerRadius(radius - 30);
	
	var pie = d3.layout.pie()
	.sort(null)
	.value(function(d) { return d.population; });
	var svg = d3.select("#middleContent"+data.index).append("svg")
	.attr("width", width)
	.attr("height", height)
	.append("g")
	.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
	
	
	
	
	graph_data.forEach(function(d) {
						d.population = +d.population;
						});
		   
		   var total= d3.sum(graph_data, function(d){return d.population;});
		   
		   var g = svg.selectAll(".arc")
		   .data(pie(graph_data))
		   .enter().append("g")
		   .attr("class", "arc");
		   
		   g.append("path")
		   .attr("d", arc)
		   .style("fill", function(d) { return color(d.data.type); });
		   
		   g.append("text")
		   .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
		   .attr("dy", ".5em")
		   .style("text-anchor", "middle")
		   .text(function(d) { return d.data.type; });



	this.floorInfoContainer = container;

	this.hideFloorInfo();
};


Floor.prototype.showFloorInfo = function() {
	this.floorInfoContainer.style.display = 'block';
};

Floor.prototype.hideFloorInfo = function() {
	this.floorInfoContainer.style.display = 'none';
};